﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace pinger
{
    public partial class Form1 : Form
    {
        private int counter;
        private static SerialPort port;
        private static bool lightOn;
        private string logFilePath = @"c:\temp\ping.txt";

        public Form1()
        {
            if (!File.Exists(logFilePath))
            {
                File.Create(logFilePath);
            }

            InitializeComponent();

            Task.Factory.StartNew(
                () =>
                    {
                        try
                        {
                            PerformPing("google.com");
                        }
                        catch
                        {
                        }
                    }
                );
        }

        private void PerformPing(string address)
        {
            while (true)
            {
                Ping ping = new Ping();

                bool result = false;
                PingReply reply = null;
                try
                {
                    reply = ping.Send(address, 5000);
                    result = reply != null && reply.Status == IPStatus.Success;
                }
                catch (Exception)
                {
                }
                finally
                {
                    ping.Dispose();
                }

                var file = File.AppendText(logFilePath);

                string replyStatus = string.Empty;
                if (reply != null)
                {
                    replyStatus = reply.Status.ToString();
                }

                long time = 0;
                if (reply != null)
                {
                    time = reply.RoundtripTime;
                }

                ToggleLight(!result);

                file.WriteLine("{0},{1},{2},{3}", DateTime.Now, replyStatus, result, time);

                file.Dispose();
                Invoke((MethodInvoker)(() =>
                {
                    chart1.Series[0].Points.Add(new DataPoint(counter, time));
                    counter++;
                }));

                Thread.Sleep(1000);
            }
        }

        private static void ToggleLight(bool internetOut)
        {
            try
            {
                if (port == null)
                {
                    port = new SerialPort("COM3", 9600);
                    port.Open();
                    port.Write("0");
                }

                if (internetOut != lightOn)
                {
                    port.Write(internetOut ? "1" : "0");
                    lightOn = internetOut;
                }
            }
            catch
            {
            }
        }
    }
}
